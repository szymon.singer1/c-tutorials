#include <iostream>
#include <Windows.h>
#include <stdlib.h>
using namespace std;

void welcome();

char getYesNo();

void printResponse(char responseToPrint);

void AskYesOrNoQuestion();

int main()
{
	// Asks user to enter y or n and thern return the response
	AskYesOrNoQuestion();

	system("pause");
}

void welcome()
{
	// welcome the user to the program
	cout << "Welcome!\n";
}

char getYesNo()
{
	// Ask the user y or n?
	cout << "Please answer: y or n.\n";

	// char variable to store the response
	char response;

	// get input from the user via keybord
	cin >> response;

	return response;
}

void printResponse(char responseToPrint)
{
	// Print the response to the screen
	cout << "Your answer was: " << responseToPrint << endl;
}

void AskYesOrNoQuestion()
{
	// Greet the user
	welcome();

	// create char variable and store the result from getYesNo()
	char answer = getYesNo(); // getYesNo() gets a y or n from the user

	// Return the response back to user on the screen 
	printResponse(answer);

}